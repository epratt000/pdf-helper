/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-has-content */
import React, { useEffect, useState, useRef } from 'react'
import { FileUpload } from 'primereact/fileupload'
import { Button } from 'primereact/button'
import { PDFDocument } from 'pdf-lib'
import { Buffer } from 'buffer'
import './App.css';

function App() {
  const pdfUploadRef = useRef(null)
  const [labelVisibility, setLabelVisibility] = useState('hidden')
  const [templateName, setTemplateName] = useState()
  const [pdfBytes, setPdfBytes] = useState()
  const [downloadName, setDownloadName] = useState('')
  const [downloadUrl, setDownloadUrl] = useState('')
  const [downloadLink, setDownloadLink] = useState()
  const [download, setDownload] = useState(false)

  useEffect(() => {
    if (download) {
      setDownload(false)
      downloadLink.click()
    }
  }, [downloadUrl])

  const downloadFile = (buffer, filename) => {
    const blob = new Blob([buffer])
    const _downloadUrl = URL.createObjectURL(blob)

    setDownload(true)
    setDownloadName(filename)
    setDownloadUrl(_downloadUrl)
  }

  const readFile = (file, callback) => {
    const reader = new FileReader()

    reader.onload = (info) => {
      const buffer = info.target.result
      const bytes = new Uint8Array(buffer)
      callback(bytes)
    }
    reader.readAsArrayBuffer(file)
  }

  const handlePDFUpload = async (e) => {
    const file = e.files[0]
    setTemplateName(file.name)
    setLabelVisibility('visible')
    readFile(file, async (result) => {
      try {
        setPdfBytes(result)
      } catch (error) {
        console.log(error)
      }
    })
    e.files = undefined
    pdfUploadRef.current.clear()
  }

  // ********************************************************************************
  // This contains all instances where I'm using pdf-lib to load a pdf and flatten it
  // ********************************************************************************
  const Flatten = async () => {
    const pdf = await PDFDocument.load(pdfBytes)
    const form = pdf.getForm()
    
    // Ensure all text fields are set so flattening works
    for (const field of form.getFields()) {
      if (field.constructor.name === 'PDFTextField') {
        field.setText('')
      }
    }

    form.flatten()
    const flatPdf = await pdf.save()

    // Trigger download
    const buffer = Buffer.from(flatPdf)
    downloadFile(buffer, 'Flattened.pdf')
  }

  return (
    <div className="App">
      <body>
        <FileUpload id="loadTemplate" ref={pdfUploadRef} mode="basic" customUpload uploadHandler={handlePDFUpload} accept=".pdf" auto chooseLabel="Load PDF"/>
        <h2 style={{visibility: labelVisibility}}>PDF: <div className="fileName">{templateName}</div></h2>
        <Button label="Flatten" onClick={Flatten} />
      </body>
      <a style={{ display: 'none' }} ref={(e) => setDownloadLink(e)} download={downloadName} href={downloadUrl}/>
    </div>
  );
}

export default App;
